package com.vaadin.starter.skeleton.more;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

@Push
@Route("pushy")
public class PushyNumbersView extends VerticalLayout {

  private Label resultFactorialLabel;
  private Label resultFibonacciLabel;
  private TextField inputTextField;
  private Button countButton;

  private UI ui;

  @Override
  protected void onAttach(AttachEvent attachEvent) {

    ui = attachEvent.getUI();

  }


  public PushyNumbersView() {
    HorizontalLayout inputWithButton = new HorizontalLayout();

    inputTextField = new TextField();
    inputTextField.setAutofocus(true);
    inputTextField.setPlaceholder("Enter a number");
    inputTextField.addValueChangeListener(event ->
        countButton.setEnabled(isValid(event.getValue()))
    );

    countButton = new Button("count");

    inputWithButton.add(inputTextField, countButton);

    resultFactorialLabel = new Label();
    resultFibonacciLabel = new Label();
    add(inputWithButton, resultFactorialLabel, resultFibonacciLabel);


    countButton.addClickListener(buttonClickEvent -> {
      int input = Integer.parseInt(inputTextField.getValue());

      long factorialResult = countFactorial(input);
      ui.access(() -> resultFactorialLabel.setText("Factorial: " + factorialResult));
      ui.push();


      try {
//        factThread.join();
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

        int fibonacciResult = countFibonacci(input);
        ui.access(() -> resultFibonacciLabel.setText("Fibonacci: " + fibonacciResult));
        ui.push();

    });
  }

  private long countFactorial(int position) {
    int result = 1;
    for (int i = 1; i <= position; ++i) {
      result *= i;
    }
    return result;
  }

  private int countFibonacci(int number) {
    if ((number == 0) || (number == 1)) {
      return number;
    } else {
      return countFibonacci(number - 1) + countFibonacci(number - 2);
    }
  }

  private static boolean isValid(String s) {
    return isInteger(s) && Integer.parseInt(s) >= 0;
  }


  private static boolean isInteger(String s) {
    if (s.isEmpty()) {
      return false;
    }

    int radix = 10;

    for (int i = 0; i < s.length(); i++) {
      if (i == 0 && s.charAt(i) == '-') {
        if (s.length() == 1) {
          return false;
        } else {
          continue;
        }
      }
      if (Character.digit(s.charAt(i), radix) < 0) {
        return false;
      }
    }
    return true;
  }

}

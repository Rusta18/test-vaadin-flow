package com.vaadin.starter.skeleton.more;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.Route;

@Route("hello/here")
public class HelloWorld extends Div {
  public HelloWorld() {
    setText("Hello Route!");
  }
}

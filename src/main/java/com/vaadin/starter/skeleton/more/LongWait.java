package com.vaadin.starter.skeleton.more;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;

@Push
@Route("long")
public class LongWait extends VerticalLayout {

  private UI ui;
  private VaadinSession session;

  public LongWait() {
    Button processButton = new Button("Process");
    processButton.addClickListener(event -> process());
    add(processButton);

    ui = UI.getCurrent();
    session = VaadinSession.getCurrent();
  }

  //should wait for 10 minutes and then add finish label
  //instead loose connection after around 5 minutes
  private void process() {

    Thread thread = new Thread(() -> {
      try {
        Thread.sleep(2 * 1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      ui.access(() -> add(new Label("finished")));

      session.lock();
      ui.push();
      session.unlock();
    });
    thread.start();
  }
}

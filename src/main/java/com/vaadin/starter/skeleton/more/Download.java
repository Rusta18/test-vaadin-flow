package com.vaadin.starter.skeleton.more;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.AbstractStreamResource;
import com.vaadin.flow.server.StreamResource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

    @Route("test-download")
    public class Download extends VerticalLayout {

      public Download() {
        try {
          addDownload();
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        }
      }

      private void addDownload() throws FileNotFoundException {
        File initialFile = new File("/home/rustam/IdeaProjects/my-first-flow-app/src/main/java/com/vaadin/starter/skeleton/more/LongWait.java");
        InputStream targetStream = new FileInputStream(initialFile);
        StreamResource stream = new StreamResource("something.java", () -> targetStream);

        Anchor downloadLink = new Anchor(stream, "Download");
        downloadLink.getElement().setAttribute("download", true);
        add(downloadLink);
      }

    }

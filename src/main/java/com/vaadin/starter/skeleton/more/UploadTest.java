package com.vaadin.starter.skeleton.more;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer;
import com.vaadin.flow.router.Route;




@Route("upload")
public class UploadTest extends VerticalLayout {
  public UploadTest(){
    MultiFileMemoryBuffer buffer = new MultiFileMemoryBuffer();
//    FileBuffer buffer = new FileBuffer();
    Upload upload = new Upload(buffer);
    add(upload);
  }
}

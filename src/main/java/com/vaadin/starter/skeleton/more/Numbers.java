package com.vaadin.starter.skeleton.more;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

import java.util.logging.Logger;

import static java.lang.Thread.sleep;


@Route("numbers")
public class Numbers extends VerticalLayout {

  private final static Logger LOGGER = Logger.getLogger(Numbers.class.getName());


  private Label resultFactorialLabel;
  private Label resultFibonacciLabel;
  private TextField inputTextField;
  private Button countButton;

//  private boolean validInput = false;


  public Numbers() {
    HorizontalLayout inputWithButton = new HorizontalLayout();

    inputTextField = new TextField();
    inputTextField.setAutofocus(true);
    inputTextField.setPlaceholder("Enter a number");
    inputTextField.addValueChangeListener(event ->
        countButton.setEnabled(isValid(event.getValue()))
    );

    countButton = new Button("count");

    inputWithButton.add(inputTextField, countButton);

    resultFactorialLabel = new Label();
    resultFibonacciLabel = new Label();
    add(inputWithButton, resultFactorialLabel, resultFibonacciLabel);


//    countButton.addClickListener(buttonClickEvent -> {
//      int input = Integer.parseInt(inputTextField.getValue());
//
//      long factorialResult = countFactorial(input);
//      resultFactorialLabel.setText("Factorial: " + factorialResult);
//
//      CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
//        try {
//          sleep(1000);
//        } catch (InterruptedException e) {
//          e.printStackTrace();
//        }
//        return countFibonacci(input);
//      });
//
//      future.thenAccept(result -> {
//        resultFibonacciLabel.setText("Fibonacci: " + result);
//        LOGGER.info("done");
//        System.out.println("done");
//      });
//    });

    countButton.addClickListener(buttonClickEvent -> {
      int input = Integer.parseInt(inputTextField.getValue());

      long factorialResult = countFactorial(input);
      resultFactorialLabel.setText("Factorial: " + factorialResult);

//      //just for example, could be processing that takes really long
//      try {
//        sleep(1000);
//      } catch (InterruptedException ignored) {
//      }

      int fibonacciResult = countFibonacci(input);
      resultFibonacciLabel.setText("Fibonacci: " + fibonacciResult);
    });
  }


  private long countFactorial(int position) {
    int result = 1;
    for (int i = 1; i <= position; ++i) {
      result *= i;
    }
    return result;
  }

  public static long[] productFib(long prod) {

    long backFibonacci = 0;    //fibonacci of F(n)
    long forwardFibonacci = 1; //fibonacci of F(n+1)

    long multiplication = backFibonacci * forwardFibonacci;  //F(n) * F(n+1)

    while (multiplication < prod) {
      long temp = backFibonacci;                          //F(n-1): F(n) on previous iteration
      backFibonacci = forwardFibonacci;                   //F(n):   F(n+1) on previous iteration
      forwardFibonacci = temp + backFibonacci;            //F(n+1): F(n-1) + F(n)
      multiplication = backFibonacci * forwardFibonacci;
    }

    long[] result = new long[3];

    result[0] = backFibonacci;
    result[1] = forwardFibonacci;
    result[2] = multiplication == prod ? 1 : 0;

    return result;
  }


  private int countFibonacci(int position) {
    if ((position == 0) || (position == 1)) {
      return position;
    } else {
      return countFibonacci(position - 1) + countFibonacci(position - 2);
    }
  }

  private static boolean isValid(String s) {
    return isInteger(s, 10) && Integer.parseInt(s) >= 0;
  }

  private static boolean isInteger(String s, int radix) {
    if (s.isEmpty()) {
      return false;
    }
    for (int i = 0; i < s.length(); i++) {
      if (i == 0 && s.charAt(i) == '-') {
        if (s.length() == 1) {
          return false;
        } else {
          continue;
        }
      }
      if (Character.digit(s.charAt(i), radix) < 0) {
        return false;
      }
    }
    return true;
  }
}
